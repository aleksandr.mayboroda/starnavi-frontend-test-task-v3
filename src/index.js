import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
import { GameContextProvider } from './context/gameContext'
import { ToastContextProvider } from './context/toastContext'

ReactDOM.render(
  <React.StrictMode>
    <GameContextProvider>
      <ToastContextProvider>
        <App />
      </ToastContextProvider>
    </GameContextProvider>
  </React.StrictMode>,
  document.getElementById('root')
)

reportWebVitals()
