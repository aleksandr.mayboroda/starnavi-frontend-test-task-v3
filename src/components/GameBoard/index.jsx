import { useContext, useEffect, useState, memo } from 'react'
import { GameContext } from '../../context/gameContext'

import {Row, Col} from 'react-bootstrap'
import GameCell from '../GameCell'

import styles from './styles.module.css'

const GameBoard = () => {
  const [cells, setCells] = useState([])

  const gameContext = useContext(GameContext)
  const activeButtons = gameContext.activeButtons
  const sizeMode = +gameContext.gameMode
  
  const isActive = (number) => activeButtons.find((el) => el.number === number)
  const toggleButton = (number) => {
    const row = Math.ceil(number / sizeMode)
    const col = number - (row * sizeMode - sizeMode)
    gameContext.toggle(row,col,number)
  }
  
  useEffect(() => {
    const arr = new Array(Math.pow(sizeMode, 2))
    setCells([...arr])
   
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sizeMode])


  return (
    <Row>
      {cells.length > 0 && (
        <Col className="game-wrapper mb-5">
          <Col className={styles['game-board']}>
            {cells.map((_, i) => ( 
              <GameCell key={i} number={i + 1} size={sizeMode} isActive={isActive} toggleButton={toggleButton} />
            ))}
          </Col>
        </Col>
      )}
    </Row>
  )
}

export default memo(GameBoard)
