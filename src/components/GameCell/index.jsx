import {memo} from 'react'
import styles from './styles.module.css'

const GameCell = ({ number, size, toggleButton, isActive }) => {
  const active = isActive(number)

  return (
    <button
      key={number}
      onMouseOver ={() => toggleButton(number)}
      className={`${styles['game-cell']} ${ active && styles.active}`}
      style={{ width: `calc(100% / ${size})`, height: `calc(100% / ${size})` }}
    >
      {/* {number} */}
    </button>
  )
}

export default memo(GameCell)
