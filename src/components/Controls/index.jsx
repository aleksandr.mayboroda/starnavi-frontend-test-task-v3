import { useState, useEffect, useContext, memo } from 'react'
import { GameContext } from '../../context/gameContext'
import { ToastContext } from '../../context/toastContext'

import CustomSelect from '../CustomSelect'

import { fetchSize } from '../../api/filters'

import { Row, Col, Form, Button } from 'react-bootstrap'

const Controls = () => {
  const gameContext = useContext(GameContext)
  const toastContext = useContext(ToastContext)

  const [options, setOptions] = useState([])
  const [formError, setFormError] = useState('')

  const mode = gameContext.gameMode
  const [size, setSize] = useState(mode)

  const onSubmit = (ev) => {
    ev.preventDefault()
    if (!size || !options.find((option) => option.value === +size)) {
      setFormError("You haven't selected the game mode!")
    } else {
      setFormError('')
    }

    gameContext.clearActiveButtons()
    gameContext.setGameMode(size)
  }

  const onSelectChange = (ev) => {
    setSize(ev.target.value)
  }

  const loadSizes = async () => {
    try {
      const response = await fetchSize()
      if (response.status === 200) {
        const result = []
        for (let key in response.data) {
          result.push({ key, value: response.data[key].field })
        }
        setOptions(result)
      }
    } catch (err) {
      toastContext.customSetShow('Server Error, shit heppens!')
    }
  }

  useEffect(() => {
    loadSizes()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <Row>
      <Form onSubmit={onSubmit} className="mb-3">
        <Row>
          <Form.Group as={Col} sx="12" sm="8" className="mb-2">
            <CustomSelect
              onSelectChange={onSelectChange}
              value={size}
              options={options}
              fieldError={formError}
            />
          </Form.Group>
          <Form.Group as={Col} sx="12" sm="2" className="mb-2">
            <Button variant="primary" type="submit">
              Start
            </Button>
          </Form.Group>
        </Row>
      </Form>
    </Row>
  )
}

export default memo(Controls)
