import React from 'react'
import {Form} from 'react-bootstrap'

const CustomSelect = ({ value, options, onSelectChange, fieldError }) => {
  const renderOptions = () =>
    options.map((option) => (
      <option key={option.key} value={option.value}>
        {option.key}
      </option>
    ))

  return (
    <>
      <Form.Select
        value={value}
        onChange={onSelectChange}
        isInvalid={fieldError}
      >
        <option value="" disabled>
          Select...
        </option>
        {options.length > 0 && renderOptions()}
      </Form.Select>
      {fieldError && (
        <Form.Control.Feedback type="invalid">
          {fieldError}
        </Form.Control.Feedback>
      )}
    </>
  )
}

export default CustomSelect
