import {memo, useContext} from 'react'
import { GameContext } from '../../context/gameContext'

import {Col} from 'react-bootstrap'
import CustomAlert from '../CustomAlert'

const GameInfo = () => {
  const gameContext = useContext(GameContext)
  const alerts = gameContext.activeButtons
  return (
    <>
      <Col sx="12">
        <h2>Hover squares</h2>
      </Col>
      <Col sx="12">
        {alerts.length > 0 &&
          [...alerts].reverse().map((oneAlert, index) => (
            <CustomAlert key={oneAlert.number} variant={index === 0 ? 'primary' : 'warning'} oneAlert={oneAlert} />
          ))}
      </Col>
    </>
  )
}

export default memo(GameInfo)
