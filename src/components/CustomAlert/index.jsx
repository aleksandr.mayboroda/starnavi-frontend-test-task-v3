import React from 'react'
import {Alert} from 'react-bootstrap'

const CustomAlert = ({oneAlert, variant}) => {
  const {row, col, number} = oneAlert
  return (
    <Alert key={number} variant={variant}>
      row {row} col {col}
    </Alert>
  )
}

export default CustomAlert
