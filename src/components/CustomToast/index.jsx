import { useContext} from 'react'
import { ToastContext } from '../../context/toastContext'
import { ToastContainer, Toast } from 'react-bootstrap'

const CustomToast = () => {
  const toastContext = useContext(ToastContext)
  return (
    <ToastContainer className="p-3" position={'bottom-start'}>
      <Toast onClose={() => toastContext.customSetShow('')} show={!!toastContext.show} delay={5000} autohide>
        <Toast.Header >
          <strong className="me-auto">Your Mom</strong>
          <small>1 bil years ago</small>
        </Toast.Header>
        <Toast.Body>{toastContext.show}</Toast.Body>
      </Toast>
    </ToastContainer>
  )
}

export default CustomToast
