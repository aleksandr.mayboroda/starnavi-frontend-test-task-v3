import { createContext, useState } from 'react'

export const initialValue = {
  buttons: [],
  mode: ''
}

export const GameContext = createContext(initialValue)

export const GameContextProvider = ({ children }) => {
  const [activeButtons, setActiveButtons] = useState(initialValue.buttons)
  const [gameMode, setGameMode] = useState(initialValue.mode)

  const clearActiveButtons = () => setActiveButtons([])

  const toggle = (row, col, number) => {
    const obj = { row, col, number }
    const existed = activeButtons.find((el) => el.number === number)
    existed
      ? setActiveButtons((prev) => prev.filter((el) => el.number !== number))
      : setActiveButtons([...activeButtons, obj])
  }
  
  return (
    <GameContext.Provider
      value={{ activeButtons, toggle, clearActiveButtons, gameMode, setGameMode }}
    >
      {children}
    </GameContext.Provider>
  )
}
