import { createContext, useState, useCallback } from 'react'

export const initialValue = {
  text: ''
}

export const ToastContext = createContext(initialValue)

export const ToastContextProvider = ({ children }) => {
  const [show, setShow] = useState(initialValue.text);

  const customSetShow = useCallback((value) => setShow(value),[])

  return (
    <ToastContext.Provider
      value={{ show, customSetShow}}
    >
      {children}
    </ToastContext.Provider>
  )
}