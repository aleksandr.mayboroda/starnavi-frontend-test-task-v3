import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import Controls from './components/Controls'
import GameBoard from './components/GameBoard'
import GameInfo from './components/GameInfo'
import CustomToast from './components/CustomToast'

function App() {
  return (
    <Container className="p-3" fluid={true}>
      <Row>
        <Col sx="12" md="7">
          <Controls />
          <GameBoard />
        </Col>
        <Col sx="12" md={{ span: 4, offset: 1 }}>
          <GameInfo />
        </Col>
      </Row>
      <CustomToast />
    </Container>
  )
}

export default App
